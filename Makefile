# hsext -- Haskell objects for Pd
# Copyright (C) 2007 Claude Heiland-Allen
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

PDSRC = $(HOME)/src/pd-0.40-2/src

CFLAGS = -I$(PDSRC) -I/opt/haskell/lib/ghc-6.6.1/include -I./build -Wall
HCFLAGS = -I$(PDSRC) -I./hsext
#HFLAGS = -odir ./build -hidir ./build -stubdir ./build -Wall -fffi
HFLAGS = -i./api -i./hsext -i./build -odir ./build -hidir ./build -Wall -fffi
#

all: build/LowLevel.hs

notall: hsext.pd_linux

clean:
	rm -f build/bootstrap.o build/HsExt.hi build/HsExt.o \
build/HsExt_stub.c build/HsExt_stub.h build/HsExt_stub.o build/libhsext.o \
build/Pd.hi build/Pd.o build/PdStructs.hi build/PdStructs.hs build/PdStructs.o \
build/PureData.hi build/PureData.o examples/Print.hi examples/Print.o \
examples/Swap.hi examples/Swap.o

dist-clean:
	rm -f hsext.pd_linux api/Pd.hi api/PureData.hi api/PdStructs.hi \
examples/Swap.hi examples/Swap.o examples/Print.hi examples/Print.o

#

hsext.pd_linux: build/bootstrap.o build/HsExt.o build/HsExt_stub.o build/Pd.o build/PdStructs.o build/libhsext.o build/PureData.o
	ghc $(HFLAGS) -no-hs-main -package plugins -optl -shared -o hsext.pd_linux build/bootstrap.o build/HsExt.o build/HsExt_stub.o build/Pd.o build/PdStructs.o build/libhsext.o build/PureData.o
	strip hsext.pd_linux

build/HsExt.o build/Pd.o build/PdStructs.o build/HsExt_stub.h build/HsExt_stub.o: hsext/HsExt.hs hsext/Pd.hs api/PureData.hs hsext/libhsext.h build/PdStructs.hs
	ghc $(HFLAGS) -no-hs-main --make -c hsext/HsExt.hs
	mv -f hsext/HsExt_stub.c build/HsExt_stub.c
	mv -f hsext/HsExt_stub.h build/HsExt_stub.h
	cp -f build/PureData.hi  api/PureData.hi
	cp -f build/PdStructs.hi api/PdStructs.hi
	cp -f build/Pd.hi        api/Pd.hi

build/PdStructs.hs: hsext/PdStructs.hsc
	hsc2hs $(HCFLAGS) -o build/PdStructs.hs hsext/PdStructs.hsc

build/libhsext.o: hsext/libhsext.c hsext/libhsext.h
	gcc $(CFLAGS) -c -o build/libhsext.o hsext/libhsext.c

build/bootstrap.o: hsext/bootstrap.c build/HsExt_stub.h
	gcc $(CFLAGS) -c -o build/bootstrap.o hsext/bootstrap.c

#

build/LowLevel.hs: hsext/LowLevel.hsc hsext/LowLevel.h
	hsc2hs $(HCFLAGS) -o build/LowLevel.hs hsext/LowLevel.hsc

