-- hsext -- Haskell objects for Pd
-- Copyright (C) 2007 Claude Heiland-Allen
-- 
-- 
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-- swap two messages

module Swap where

import Data.IORef
import PureData

creator :: Creator
creator args = do
  bang <- gensym "bang"
  state <- newIORef (bang, [])
  return (Just Instance{iInlets = 2, iOutlets = 2, iMethod = inlet state})

inlet state outlet 0 s m = do
  (rs,rm) <- readIORef state
  outlet 1 s m
  outlet 0 rs rm
  return ()

inlet state outlet 1 s m = do
  writeIORef state (s,m)
  return ()
