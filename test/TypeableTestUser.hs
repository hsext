module TypeableTestUser ( main ) where

import Data.Typeable
import TypeableTest

main = print . show $ typeOf (FooFloat 1.4)
