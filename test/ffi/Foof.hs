module Foof where

foreign export ccall foof :: Float -> IO Float

foof :: Float -> IO Float
foof n = return (fromIntegral (length (f n)))

f :: Float -> [Float]
f n
  | n<1       = []
  | otherwise = n:(f (n-1))
