#include <stdio.h>
#include <HsFFI.h>

#include "Barf.h"

#ifdef __GLASGOW_HASKELL__
#include "Foof_stub.h"
#endif

#ifdef __GLASGOW_HASKELL__
extern void __stginit_Foof ( void );
#endif

int test(int argc, char *argv[])
{
  int i;

  hs_init(&argc, &argv);
#ifdef __GLASGOW_HASKELL__
  hs_add_root(__stginit_Foof);
#endif

  for (i = 0; i < 5; i++) {
    printf("%e\n", (double) foof(2500.0));
  }

  hs_exit();
  return 0;
}

