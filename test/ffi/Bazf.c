#include "Barf.h"

#include <stdio.h>
#include <dlfcn.h>


int main(int argc, char *argv[]) {
    void *bar;
    int (*test)(int*,char**[]);
    fprintf(stderr, "Loading 'Barf.so'... ");
    bar = dlopen("./Barf.so", RTLD_NOW);
    if (!bar) {
        fprintf(stderr, "FAILED!\n");
        return 1;
    }
    fprintf(stderr, "OK!\n");
    fprintf(stderr, "Looking up 'test'... ");
    test = dlsym(bar, "test");
    if (!test) {
        fprintf(stderr, "FAILED!\n");
        dlclose(bar);
        return 2;
    }
    fprintf(stderr, "OK!\n");
    fprintf(stderr, "Running 'test'...\n");
    test(&argc, &argv);
    fprintf(stderr, "Test completed!\n");
    dlclose(bar);
    return 0;
}
