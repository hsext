{-# OPTIONS_GHC -fglasgow-exts #-}

module TypeableTest ( Foo(FooFloat, FooString) ) where

import Data.Typeable

data Foo = FooFloat Float | FooString String
  deriving (Read, Show, Eq, Typeable)
