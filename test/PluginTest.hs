import System.Plugins
import PureData

src     = "../examples/Example1.hs"
apipath = "../api"

main = do status <- make src ["-i"++apipath]
          case status of
                MakeSuccess _ _ -> f
                MakeFailure e -> mapM_ putStrLn e

  where f = do v <- pdynload "../examples/Example1.o" [apipath] [] "PureData.Class" "setup"
               case v of
                  LoadSuccess _ a  -> putStrLn "loaded .. yay!"
                  _           -> putStrLn "wrong types"

