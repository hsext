#ifndef LOADER_H
#define LOADER_H 1
#include <m_pd.h>
typedef int (*loader_t)(t_canvas *, char *);
extern void sys_register_loader(loader_t loader);
#endif
