/*
hsext -- Haskell objects for Pd
Copyright (C) 2007 Claude Heiland-Allen


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef LIBHSEXT_H
#define LIBHSEXT_H 1

#include <m_pd.h>

/* ~~~~ each instance of hsext is one of these in C-space ~~~~ */
typedef struct _libhsext {
    t_object x_obj;        /* just because Pd likes it this way */
    t_int hs_id;           /* passed by Haskell-space creator */
    t_int hs_inletcount;   /* passed by Haskell-space creator */
    t_int hs_receivecount; /* passed by Haskell-space creator */
    t_int hs_outletcount;  /* passed by Haskell-space creator */
    struct _libhsext_inlet   *hs_inlets;   /* we manage our inlets   */
    struct _libhsext_receive *hs_receives; /* we manage our receives */
    t_outlet                **hs_outlets;  /* we manage our outlets  */
} t_libhsext;

/* ~~~~ one of these for each inlet of a given hsext instance ~~~~ */
typedef struct _libhsext_inlet {
    t_pd hsi_owner;    /* this is set to a proxy class */
    t_int hsi_id;      /* passed by Haskell-space creator, == hs_id */
    t_int hsi_number;  /* 0 = leftmost, 1 = next-from-leftmost, ... */
} t_libhsext_inlet;

/* ~~~~ one of these for each receiver of a given hsext instance ~~~~ */
typedef struct _libhsext_receive {
    t_pd hsr_owner;    /* this is set to a proxy class */
    t_int hsr_id;      /* passed by Haskell-space creator, == hs_id */
    t_int hsr_number;  /* from the order in which names were passed */
} t_libhsext_receive;

/* ~~~~ function pointer types ~~~~ */
typedef void (t_newf  )(t_symbol *, int, t_atom *);
typedef void (t_freef )(t_libhsext *);
typedef void (t_inletf)(int, int, t_symbol *, int, t_atom *);
typedef void (t_receivef)(int, int, t_symbol *, int, t_atom *);

/* ~~~~ called from Haskell-space ~~~~ */
extern void libhsext_setup(
    t_newf newf, t_freef freef, t_inletf inletf, t_receivef receivef
);
extern t_libhsext *libhsext_new(
    int id, int inlets, int outlets, int receives, t_symbol **receivenames
);
extern void libhsext_free(t_libhsext *hs);
extern void libhsext_outlet(
    t_libhsext *hs, int o, t_symbol *s, int argc, t_atom *argv
);
extern void libhsext_find(const char *name, unsigned int size, char *result);

#endif
