-- hsext -- Haskell objects for Pd
-- Copyright (C) 2007 Claude Heiland-Allen
-- 
-- 
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


module HsExt where

import Data.IORef
import Data.Map(Map)
import qualified Data.Map as Map
import Foreign.C.Types
import Foreign.C.String
import Foreign.Ptr
import Foreign.Marshal.Array
import Foreign.Marshal.Alloc (allocaBytes)

import System.Plugins.Make (
  make,
  MakeStatus(MakeFailure,MakeSuccess))

import System.Plugins.Load (
  pdynload,
  LoadStatus(LoadFailure,LoadSuccess))

import Pd

import PureData -- API

type InstanceID  = CInt

type InletNumber = CInt

type CallStack   = [InstanceID]

data State       = State {
  sCreator  :: Map (Ptr Symbol) Creator,
  sInstance :: Map InstanceID Instance,
  sObject   :: Map InstanceID (Ptr Object),
  sNextId   :: InstanceID}

type NewMethod =
  Ptr Symbol -> CInt -> Ptr Atom -> IO (Ptr Object)
foreign import ccall "wrapper"
  wrapNew :: NewMethod -> IO (FunPtr NewMethod)

type FreeMethod =
  Ptr Object -> IO ()
foreign import ccall "wrapper"
  wrapFree :: HsExt.FreeMethod -> IO (FunPtr HsExt.FreeMethod)

type InletMethod = 
  InstanceID -> InletNumber -> Ptr Symbol -> CInt -> Ptr Atom -> IO ()
foreign import ccall "wrapper"
  wrapInlet :: InletMethod -> IO (FunPtr InletMethod)


foreign import ccall "libhsext.h libhsext_setup"
  libhsext_setup :: (FunPtr NewMethod) -> (FunPtr HsExt.FreeMethod)
                 -> (FunPtr InletMethod) -> IO ()

foreign import ccall "libhsext.h libhsext_new"
  libhsext_new :: CInt -> CInt -> CInt -> IO (Ptr Object)

foreign import ccall "libhsext.h libhsext_free"
  libhsext_free :: Ptr Object -> IO CInt

foreign import ccall "libhsext.h libhsext_outlet"
  libhsext_outlet :: Ptr Object -> CInt -> Ptr Symbol -> CInt -> Ptr Atom
                  -> IO ()

foreign import ccall "libhsext.h libhsext_find"
  libhsext_find :: CString -> CUInt -> Ptr CChar -> IO ()


foreign export ccall hsext_init :: Setup
hsext_init :: Setup
hsext_init = do
  sref <- newIORef s0
  newMethod <- wrapNew (new sref)
  freeMethod <- wrapFree (free sref)
  inletMethod <- wrapInlet (inlet sref)
  libhsext_setup newMethod freeMethod inletMethod
  return ()
  where
    s0 = State{
      sCreator = Map.empty,
      sInstance = Map.empty,
      sObject = Map.empty,
      sNextId = 1}

    new :: IORef State -> NewMethod
    new sref _ argc argv
      | argc > 0 = do
        msg <- peekArray (fromIntegral argc) argv
        case (head msg) of
          ASymbol sel -> do
            mbc <- loader sref sel
            s <- readIORef sref
            case mbc of
              Nothing -> do
                return nullPtr
              Just c -> do
                mbins <- c (tail msg)
                case mbins of
                  Nothing -> do
                    return nullPtr
                  Just ins -> do
                    pd <- libhsext_new (sNextId s) (iInlets ins) (iOutlets ins)
                    writeIORef sref s{
                      sInstance = Map.insert (sNextId s) ins (sInstance s),
                      sObject   = Map.insert (sNextId s) pd  (sObject s),
                      sNextId   = (sNextId s) + 1}
                    return pd
          _ -> do return nullPtr
      | otherwise = do return nullPtr

    free :: IORef State -> HsExt.FreeMethod
    free sref pd = do
      s <- readIORef sref
      insId <- libhsext_free pd
      writeIORef sref s{
        sObject   = Map.delete insId (sObject   s),
        sInstance = Map.delete insId (sInstance s)}

    inlet :: IORef State -> InletMethod
    inlet sref insId n sel argc argv = do
      s <- readIORef sref
      case (Map.lookup insId (sInstance s)) of
        Nothing -> do
          putStrLn "hsext.inlet: can't find instance"
        Just ins -> do
          msg <- peekArray (fromIntegral argc) argv
          (iMethod ins) (outlet sref insId) n sel msg

    outlet :: IORef State -> InstanceID -> Outlet
    outlet sref insId n sel msg = do
      s <- readIORef sref
      case (Map.lookup insId (sObject s)) of
        Nothing -> do
          putStrLn "hsext.outlet: can't find object"
        Just pd -> do
          withArrayLen msg (\argc argv ->
            libhsext_outlet pd n sel (fromIntegral argc) argv)

-- TODO: use Pd's path
    loader :: IORef State -> Ptr Symbol -> IO (Maybe Creator)
    loader sref sel = do
      s <- readIORef sref
      apipath <- getApiPath
      case (Map.lookup sel (sCreator s)) of
        Just c  -> do
          return (Just c)
        Nothing -> do
          filename <- getSource sel    -- "-v" in make options breaks things
          makestatus <- make filename ("-O2":(map ("-i" ++) apipath))
          case makestatus of
            MakeFailure e -> do
              putStrLn "hsext: MakeFailure"
              mapM_ putStrLn e
              return Nothing
            MakeSuccess _ o -> do
              loadstatus <- pdynload o apipath [] "PureData.Creator" "creator"
              case loadstatus of
                LoadFailure e -> do
                  putStrLn "hsext: LoadFailure"
                  mapM_ putStrLn e
                  return Nothing
                LoadSuccess _ c -> do
                  writeIORef sref s{sCreator = Map.insert sel c (sCreator s)}
                  return (Just c)
      where
        getApiPath = do
          ps <- peekPath
          return (ps ++ (map (++ "/api") ps))
        getSource s = do
          name <- peekSymbol s
          dir <- allocaBytes 1024 (\p -> do
            withCString name (\cname -> libhsext_find cname 1024 p)
            d <- peekCString p
            return d)
          return (dir ++ "/" ++ name ++ ".hs") -- TODO: platform dependent
