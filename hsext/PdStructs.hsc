{-# OPTIONS -fffi #-}
-- hsext -- Haskell objects for Pd
-- Copyright (C) 2007 Claude Heiland-Allen
-- 
-- 
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

module PdStructs (
  peekSymbol,
  gensym,
  Symbol,
  Atom(AFloat,ASymbol),
  peekPath,
) where

#include "m_pd.h"
#include "s_stuff.h"

import Foreign.C.Types
import Foreign.C.String
import Foreign.Ptr
import Foreign.Storable

data PdVersion = PdVersion {
  majorVersion  :: Int,
  minorVersion  :: Int,
  bugfixVersion :: Int,
  testVersion   :: String }
    deriving (Show, Read, Eq, Ord)
  
compiledAgainst :: PdVersion
compiledAgainst = PdVersion {
  majorVersion  = (#const PD_MAJOR_VERSION),
  minorVersion  = (#const PD_MINOR_VERSION),
  bugfixVersion = (#const PD_BUGFIX_VERSION),
  testVersion   = (#const_str PD_TEST_VERSION) }

maxPdString :: Int
maxPdString = (#const MAXPDSTRING)

maxPdArg :: Int
maxPdArg = (#const MAXPDARG)

type PdInt = (#type t_int)
type PdFloat = (#type t_float)
type PdFloatArg = (#type t_floatarg)

data PdSymbol = PdSymbol {
  sName  :: Ptr CChar,
  sThing :: Ptr (Ptr PdClass),
  sNext  :: Ptr PdSymbol }

instance Storable PdSymbol where
  sizeOf    _ = (#size struct _symbol)
  alignment _ = 1    -- FIXME
  peek sp     = do
    name  <- (#peek struct _symbol, s_name)  sp
    thing <- (#peek struct _symbol, s_thing) sp
    next  <- (#peek struct _symbol, s_next)  sp
    return (PdSymbol { sName = name, sThing = thing, sNext = next })
  poke sp  (PdSymbol { sName = name, sThing = thing, sNext = next }) = do
    (#poke struct _symbol, s_name)  sp name
    (#poke struct _symbol, s_thing) sp thing
    (#poke struct _symbol, s_next)  sp next


foreign import ccall "m_pd.h gensym" c_gensym
  :: CString -> IO (Ptr PdSymbol)

gensym :: String -> IO (Ptr PdSymbol)
gensym s = withCString s c_gensym



type PdGStubType = (#type int)

stubNone  :: PdGStubType
stubNone  = (#const GP_NONE)

stubGList :: PdGStubType
stubGList = (#const GP_GLIST)

stubArray :: PdGStubType
stubArray = (#const GP_ARRAY)

data PdGStub = PdGStubNone  { gsRefCount :: CInt }
             | PdGStubGList { gsRefCount :: CInt, gsGList :: (Ptr PdGList) }
             | PdGStubArray { gsRefCount :: CInt, gsArray :: (Ptr PdArray) }

instance Storable PdGStub where
  sizeOf    _ = (#size struct _gstub)
  alignment _ = 1    -- FIXME
  peek gsp = do
    glist    <- (#peek struct _gstub, gs_un.gs_glist) gsp
    array    <- (#peek struct _gstub, gs_un.gs_array) gsp
    which    <- (#peek struct _gstub, gs_which)       gsp
    refCount <- (#peek struct _gstub, gs_refcount)    gsp
    return (case which of
      | stubNone  -> PdGStubNone  { gsRefCount = refcount })
      | stubGList -> PdGStubGList { gsRefCount = refcount, gsGList glist })
      | stubArray -> PdGStubArray { gsRefCount = refcount, gsArray array }) )
  poke gsp (PdGStubNone  { gsRefCount = refcount }) = do
    (#poke struct _gstub, gs_which)       gsp stubNone
    (#poke struct _gstub, gs_refcount)    gsp refcount
  poke gsp (PdGStubGList { gsRefCount = refcount, gsGList = glist }) = do
    (#poke struct _gstub, gs_un.gs_glist) gsp glist
    (#poke struct _gstub, gs_which)       gsp stubGList
    (#poke struct _gstub, gs_refcount)    gsp refcount
  poke gsp (PdGStubArray { gsRefCount = refcount, gsArray = array }) = do
    (#poke struct _gstub, gs_un.gs_array) gsp glist
    (#poke struct _gstub, gs_which)       gsp stubArray
    (#poke struct _gstub, gs_refcount)    gsp refcount


data PdGPointer = PdGPointer

instance Storeable PdGPointer where
  sizeOf    _ = (#size struct _gpointer)
  alignment _ = 1    -- FIXME
  peek      _ = error "peek unimplemented for 'struct _gpointer' (Storeable PdGPointer)"
  poke    _ _ = error "poke unimplemented for 'struct _gpointer' (Storeable PdGPointer)"

typedef union word
{
    t_float w_float;
    t_symbol *w_symbol;
    t_gpointer *w_gpointer;
    t_array *w_array;
    struct _glist *w_list;
    int w_index;
} t_word;

typedef enum
{
    A_NULL,
    A_FLOAT,
    A_SYMBOL,
    A_POINTER,
    A_SEMI,
    A_COMMA,
    A_DEFFLOAT,
    A_DEFSYM,
    A_DOLLAR, 
    A_DOLLSYM,
    A_GIMME,
    A_CANT
}  t_atomtype;

#define A_DEFSYMBOL A_DEFSYM    /* better name for this */

typedef struct _atom
{
    t_atomtype a_type;
    union word a_w;
} t_atom;

data Atom = AFloat CFloat
          | ASymbol (Ptr Symbol)
--  deriving (Eq, Show)


instance Storable Atom where

  sizeOf _ = (#size struct _atom)

  alignment _ = 1    -- FIXME

  peek p = do
    t <- ((#peek struct _atom, a_type) p)::IO CInt
    case t of

      (#const A_FLOAT) -> do
        f <- (#peek struct _atom, a_w.w_float) p
        return (AFloat f)

      (#const A_SYMBOL) -> do
        s <- (#peek struct _atom, a_w.w_symbol) p
        return (ASymbol s)

      _ -> error "Atom.peek: unsupported atom type"

  poke p (AFloat f) = do
    (#poke struct _atom, a_type) p ((#const A_FLOAT)::CInt)
    (#poke struct _atom, a_w.w_float) p f

  poke p (ASymbol s) = do
    (#poke struct _atom, a_type) p ((#const A_SYMBOL)::CInt)
    (#poke struct _atom, a_w.w_symbol) p s

  poke _ _ = error "Atom.poke: unsupported atom type"


-- typedef struct _namelist    /* element in a linked list of stored strings */
-- {
--     struct _namelist *nl_next;  /* next in list */
--     char *nl_string;            /* the string */
-- } t_namelist;
-- extern t_namelist *sys_searchpath;

type NameList = ()

nlNext :: Ptr NameList -> IO (Ptr NameList)
nlNext p = (#peek struct _namelist, nl_next) p
nlName :: Ptr NameList -> IO (CString)
nlName p = (#peek struct _namelist, nl_string) p

foreign import ccall "s_stuff.h &sys_searchpath" raw_searchpath
  :: Ptr (Ptr NameList)

peekPath :: IO [String]
peekPath = do
  p <- peek raw_searchpath
  f p []
  where
    f lp ps
      | lp == nullPtr = do
        return ps
      | otherwise     = do
        nlp <- nlNext lp
        q   <- nlName lp
        s   <- peekCString q
        ss  <- f nlp ps
        return (s:ss)
