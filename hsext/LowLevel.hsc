{-# OPTIONS -fffi #-}
-- hsext -- Haskell objects for Pd
-- Copyright (C) 2007 Claude Heiland-Allen
-- 
-- 
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


module LowLevel (
  Class,
  Object,
  InletProxy,
  InletNumber,
  Inlet,
  OutletStruct,
  Outlet,
) where

#include "m_pd.h"
#include "s_stuff.h"

#include "LowLevel.h"

data Class = Class

data Object = Object

type InletProxy  = Ptr Class

type InletNumber = (#type int)

data Inlet  = Inlet InletProxy (Ptr Object) InletNumber
instance Storable Inlet where
  alignment = 1
  sizeOf _ = (#size struct hsinlet)
  peek inlet = do
    proxy  = (#peek struct hsinlet, proxy)  inlet
    owner  = (#peek struct hsinlet, owner)  inlet
    number = (#peek struct hsinlet, number) inlet
    return (Inlet proxy owner number)
  poke inlet (Inlet proxy owner number) = do
    (#poke struct hsinlet, proxy)  inlet proxy
    (#poke struct hsinlet, owner)  inlet owner
    (#poke struct hsinlet, number) inlet number

data OutletStruct = OutletStruct

data Outlet = Outlet (Ptr OutletStruct)
instance Storable Outlet where
  alignment = 1
  sizeOf _ = (#size struct hsoutlet)
  peek outlet = do
    outletptr = (#peek struct hsoutlet, outlet) outlet
    return (Outlet outletptr)
  poke outlet (Outlet outletptr) = do
    (#poke struct hsoutlet, outlet) outlet outletptr

