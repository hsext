module Dispatcher (
  Dispatcher,  -- abstract data type
  empty,       -- an empty dispatcher
  fallback,    -- a method to call when type unrecognized
  insert,      -- associate a method to a type
  delete,      -- disassociate a method from a type
  dispatch     -- dispatch methods according to type
) where

import Data.Dynamic
import Data.Typeable
import Data.Maybe
import Data.IntMap(IntMap)
import qualified Data.IntMap as IntMap

data Dispatcher = D (IntMap Dynamic) (Maybe (Dynamic -> IO ()))

empty :: Dispatcher
empty = D IntMap.empty Nothing

fallback :: Maybe (Dynamic -> IO ()) -> Dispatcher -> IO Dispatcher
fallback f (D d _) = return (D d f)

insert :: (Typeable a) => a -> (a -> IO ()) -> Dispatcher -> IO Dispatcher
insert t m (D d f) = typeRepKey (typeOf t)
  >>= \k -> return (D (IntMap.insert k (toDyn m) d) f)

delete :: (Typeable a) => a -> Dispatcher -> IO Dispatcher
delete t (D d f) = typeRepKey (typeOf t)
  >>= \k -> return (D (IntMap.delete k d) f)

dispatch :: Dynamic -> Dispatcher -> IO ()
dispatch t (D d f) = do
  key <- typeRepKey (dynTypeRep t)
  case (IntMap.lookup key d) of
    Nothing -> case f of
      Nothing -> putStrLn "Dispatcher.dispatch: no fallback"
      Just f' -> f' t
    Just method -> case (dynApply method t) of
      Nothing -> putStrLn "Dispatcher.dispatch: dynApply -> Nothing"
      Just r -> case (fromDynamic r) of
        Nothing -> putStrLn "Dispatcher.dispatch: fromDynamic -> Nothing"
        Just r' -> r'

test = return empty
  >>= fallback (Just (\x -> putStrLn ("WTF I has a " ++ show x ++ "!")))
  >>= insert (undefined::Integer) (putStrLn . ("I has a Integer! "++) . show)
  >>= insert (undefined::String) (putStrLn . ("I has a String!  "++) . show)
  >>= insert (undefined::[Int]) (putStrLn . ("I has a [Int]!   "++) . show)
  >>= \d -> do
    dispatch (toDyn (1::Int)) d
    dispatch (toDyn ("hello"::String)) d
    dispatch (toDyn (1::Integer)) d
    dispatch (toDyn ([0..9]::[Int])) d
    delete (undefined::String) d
      >>= fallback Nothing
      >>= dispatch (toDyn ['H','i'])
