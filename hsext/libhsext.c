/*
hsext -- Haskell objects for Pd
Copyright (C) 2007 Claude Heiland-Allen


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "libhsext.h"

#include <unistd.h>
#include <string.h>

/* ~~~~ global variables ~~~~ */
static t_class    *libhsext_inlet_class;   /* proxy inlet class */
static t_class    *libhsext_receive_class; /* proxy receive class */
static t_class    *libhsext_class;         /* real class */
static t_inletf   *libhsext_inletf;        /* inlet dispatcher */
static t_receivef *libhsext_receivef;      /* receive dispatcher */

/* ~~~~ anything method for our proxy inlet ~~~~ */
static void libhsext_inlet(
    t_libhsext_inlet *hsi, t_symbol *s, int argc, t_atom *argv
) {
    /* dispatch based on proxy inlet contents */
    (libhsext_inletf)(hsi->hsi_id, hsi->hsi_number, s, argc, argv);
}

/* ~~~~ anything method for our proxy receive ~~~~ */
static void libhsext_receive(
    t_libhsext_receive *hsr, t_symbol *s, int argc, t_atom *argv
) {
    /* dispatch based on proxy receive contents */
    (libhsext_receivef)(hsr->hsr_id, hsr->hsr_number, s, argc, argv);
}

/* ~~~~ set up C stuff ~~~~ */
extern void libhsext_setup(
    t_newf newf, t_freef freef, t_inletf inletf, t_receivef receivef
) {
    /* create proxy inlet class */
    libhsext_inletf = inletf;
    libhsext_inlet_class = class_new(
        gensym("hsext inlet"), 0, 0, sizeof(t_libhsext_inlet), 0, 0
    );
    class_addanything(libhsext_inlet_class, libhsext_inlet);

    /* create proxy receive class */
    libhsext_receivef = receivef;
    libhsext_receive_class = class_new(
        gensym("hsext receive"), 0, 0, sizeof(t_libhsext_receive),
        CLASS_NOINLET, 0
    );
    class_addanything(libhsext_receive_class, libhsext_receive);

    libhsext_class = class_new(  /* create hsext class */
        gensym("hsext"),
        (t_newmethod) newf,
        (t_method) freef,
        sizeof(t_libhsext),
        CLASS_NOINLET,     /* inlets defined on a per-instance basis */
        A_GIMME,           /* the raw unadulterated truth */
        0
    );
}

/* ~~~~ constructor ~~~~ */
extern t_libhsext *libhsext_new(
    int id, int inlets, int outlets, int receives, t_symbol **receivenames
) {
    t_libhsext *hs = NULL;
    int i, o;
    hs = (t_libhsext *) pd_new(libhsext_class);
    if (!hs) goto bork;
    hs->hs_id           = id;
    hs->hs_inletcount   = inlets;
    hs->hs_receivecount = receives;
    hs->hs_outletcount  = outlets;
    hs->hs_inlets       = NULL; /* lest we bork */
    hs->hs_receives     = NULL; /* lest we bork */
    hs->hs_outlets      = NULL; /* lest we bork */

    /* create proxy inlets */
    if (hs->hs_inletcount > 0) {
        if (!(hs->hs_inlets = (t_libhsext_inlet *)
            getbytes(hs->hs_inletcount * sizeof(t_libhsext_inlet)))) goto bork;
        for (i = 0; i < hs->hs_inletcount; i++) {
            hs->hs_inlets[i].hsi_owner  = libhsext_inlet_class;
            hs->hs_inlets[i].hsi_id     = hs->hs_id;
            hs->hs_inlets[i].hsi_number = i;
            if (!(inlet_new(&hs->x_obj, &hs->hs_inlets[i].hsi_owner, 0, 0)))
                goto bork;
        }
    }

    /* create proxy receives */
/*    if (hs->hs_receivecount > 0) {
        if (!(hs->hs_receives = (t_libhsext_receive *)
            getbytes(hs->hs_receivecount * sizeof(t_libhsext_receive)))) goto bork;
        for (i = 0; i < hs->hs_receivecount; i++) {
            hs->hs_receives[i].hsr_owner  = libhsext_receive_class;
            hs->hs_receives[i].hsr_id     = hs->hs_id;
            hs->hs_receives[i].hsr_number = i;
            pd_bind(&hs->x_obj.ob_pd, s);
void pd_bind(t_pd *x, t_symbol *s)

        }
    }
*/

    /* create outlets */
    if (hs->hs_outletcount > 0) {
        if (!(hs->hs_outlets = (t_outlet **)
            getbytes(hs->hs_outletcount * sizeof(t_outlet *)))) goto bork;
        for (o = 0; o < hs->hs_outletcount; o++) {
            if (!(hs->hs_outlets[o] = outlet_new(&hs->x_obj, 0))) goto bork;
        }
    }

    return (hs);
bork: /* something went wrong */
    libhsext_free(hs);
    hs = NULL;
    return (NULL);
}

/* ~~~~ destructor ~~~~ */
extern void libhsext_free(t_libhsext *hs) {
    int o;
    if (hs) {
        if (hs->hs_inlets) { /* don't know if the following is safe */
            freebytes(hs->hs_inlets, hs->hs_inletcount * sizeof(t_libhsext_inlet));
            hs->hs_inlets = NULL;
        }
        if (hs->hs_outlets) { /* free outlets */
            for (o = 0; o < hs->hs_outletcount; o++) {
                if (hs->hs_outlets[o]) {
                    outlet_free(hs->hs_outlets[o]);
                    hs->hs_outlets[o] = NULL;
                } else {
                    break; /* outlets allocated in order */
                }
            }
            freebytes(hs->hs_outlets, hs->hs_outletcount * sizeof(t_outlet *));
            hs->hs_outlets = NULL;
        }
    }
}

/* ~~~~ send anything to one of our outlets ~~~~ */
extern void libhsext_outlet(
    t_libhsext *hs, int o, t_symbol *s, int argc, t_atom *argv
) {
    if (hs) {
        if (0 <= o && o < hs->hs_outletcount) {
            if (hs->hs_outlets) {
                if (hs->hs_outlets[o]) {
                    outlet_anything(hs->hs_outlets[o], s, argc, argv);
                    return;
                }
            }
        }
    }
    post("hsext_outlet_wrapper: null pointer somewhere along the line");
}

/* ~~~~ helper for finding a .hs or .lhs file in Pd's path ~~~~ */
extern void libhsext_find(const char *name, unsigned int size, char *result) {
    int fd;
    char *nameresult;
    fd = open_via_path("", name, ".hs", result, &nameresult, size, 0);
    if (fd > 0) {
        close(fd);
    } else {
        nameresult = NULL;
        strcpy(result, "."); /* not found in path, last resort */
    }
    if (result == nameresult) {
        strcpy(result, "."); /* found in current dir */
    }
}
