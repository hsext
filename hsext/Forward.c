/*
hsext -- Haskell objects for Pd
Copyright (C) 2007 Claude Heiland-Allen


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <m_pd.h>

typedef (struct hsobject *)(t_hsobject_constructor)(
  t_symbol *, int, t_atom *
);

typedef void (t_hsobject_destructor)(
  struct hsobject *
);

typedef void (t_hsinlet_dispatcher)(
  struct hsobject *, struct hsinlet *, t_symbol *, int, t_atom *
);

static t_class *hsinlet_class;
static t_class *hsobject_class;

extern void hsobject_setup(
  t_hsobject_constructor constructor,
  t_hsobject_destructor destructor,
  t_hsinlet_dispatcher dispatcher
) {
  /* create proxy inlet class */
  hsinlet_class = class_new(
    gensym("hsext inlet"), 0, 0, sizeof(struct hsinlet), 0, 0
  );
  class_addanything(hsinlet_class, dispatcher);
  /* create hsext class */
  hsobject_class = class_new(
    gensym("hsext"),
    (t_newmethod) constructor,
    (t_method) destructor,
    sizeof(t_libhsext),
    CLASS_NOINLET,
    A_GIMME,
    0
  );
}


  post("lua: (GPL) 2007 Claude Heiland-Allen <claudiusmaximus@goto10.org>");
  L = lua_open();
  luaL_openlibs(L);
  Pd_Init(L);
  char buf[MAXPDSTRING];
  char *ptr;
  t_pdlua_readerdata reader;
  /* file "pd.lua" is some glue to ease the pain of SWIG */
  int fd = canvas_open(0, "pd", ".lua", buf, &ptr, MAXPDSTRING, 1);
  if (fd >= 0) {
    reader.fd = fd;
    if (lua_load(L, pdlua_reader, &reader, "pd.lua") ||
        lua_pcall(L, 0, 0, 0)) {
      post(
        "lua: error loading `pd.lua': %s",
        lua_tostring(L, -1)
      );
      post("lua: loader will not be registered!");
      lua_pop(L, 1);
      close(fd);
    } else {
      close(fd);
      sys_register_loader(pdlua_loader);
    }
  } else {
    post("lua: error loading `pd.lua': canvas_open() failed");
    post("lua: loader will not be registered!");
  }
