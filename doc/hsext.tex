\documentclass{NIME-alternate}

\usepackage{graphicx}

\begin{document}

\title{hsext}
\subtitle{[Draft Version]}
\date{27 March 2007}

\numberofauthors{1}

\author{
\alignauthor Claude Heiland-Allen\\
\affaddr{GOTO10}\\
\email{claudiusmaximus@goto10.org}
}

\maketitle

\begin{abstract}

hsext is a PureData external library for loading and using within PureData code
written in the Haskell\footnote{http://www.haskell.org} programming language.

Haskell is a non-strict, purely-functional programming language, with
polymorphic typing, lazy evaluation and higher-order functions.

This paper aims to introduce and discuss the motivation, implementation and use
of hsext, and also present a roadmap for future development.

\end{abstract}

\keywords{PureData, external, Haskell}


\section{Introduction} 

The birth of hsext can be traced to late 2006, when the author returned to
Haskell after several years' abscence.  Development of hsext started in January
2007, and hsext-0.1 was tagged on 14th March 2007.

hsext-0.1 is sufficient to write simple externals, such as a generic ``Swap''
external that can swap arbitrary messages (Pd's internal ``swap'' is specific to
``float'' messages).


\section{Motivation: Why Haskell?}

C, Python, Java, and so on, are all imperative languages, wherein a program
consists of a sequence of commands describing how to compute something.  Haskell
is a pure functional language, wherein a program consists of a single expression
describing what needs to be computed.  This focus on the high-level ``what''
rather than the low-level ``how'' is a distinguishing characteristic of functional
programming languages.

The ``purity'' of Haskell -- the explicit separation of
execution of ``actions'' (which may have side effects) and evaluation of
expressions (which may not) -- allows a good compiler to make seemingly
astonishing optimizations\footnote{ClaudiusMaximus: Compiling with -O2 reduced
the time taken by my program's execution from 28 mins to 17 seconds. -- Haskell
Weekly News: January 31, 2007: Quotes of the Week
http://sequence.complete.org/hwn/20070131}.

Haskell is non-strict, using lazy evaluation by default.  This allows infinite
data structures to be defined, and evaluated just as much as is necessary (but
no more).  The classic example is the simple definition of the Fibonacci
sequence\footnote{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ... where each successive
number is the sum of the previous two numbers 
http://www.research.att.com/~njas/sequences/A000045} by:

\begin{verbatim}
fibs = 0 : 1 : zipWith (+) fibs (tail fibs)
\end{verbatim}


\section{Implementation: Weaving from C to Haskell and back again}

Haskell has a Run Time System (RTS) which must be started before any Haskell
code can be used.  When compiling standalone programs this is handled
automatically by the Haskell compiler, but for shared libraries such as Pd
externals there is not such compiler support (as of ghc-6.6, the current stable
release of the most prevalent Haskell compiler).  This prevents hsext being
written solely in Haskell, and so there is a small amount of C code that starts
the RTS and calls the main hsext entry point (written in Haskell) (Figure
\ref{F:setup}).

\begin{figure} 
  \centering
  \includegraphics{setup}
  \caption{Control flow when loading hsext.}
  \label{F:setup}
\end{figure}

Haskell has a Foreign Function Interface (FFI) which allows Haskell to call C
functions, and also allows Haskell functions to be ``wrapped'' into C function
pointers that are able to be called from C code.  The tool hsc2hs helps write
Haskell to manipulate C values (including defines and structs), but the author
found it more comfortable to write small helper functions (libhsext) in C to
abstract some of the more idiosyncratic aspects of Pd's API from the Haskell
part of hsext.

Haskell has a package for dynamically compiling and loading Haskell code at
runtime (Plugins).  When an hsext object is instantiated in Pd, the first
creation argument is used to form the name of a Haskell source code file to
compile and load.  This Haskell source code defines a ``resource'', which must be
of type ``Creator'' (which is defined by the ``PureData'' API provided by hsext),
and the remaining creation arguments are passed to it.  The creator resource in
hsext-0.1 can reject the arguments, or it may return an ``Instance'' specifying
the number of inlets and outlets, and a single function, which will handle all
messages arriving at the inlets (Figure \ref{F:create}).

\begin{figure} 
  \centering
  \includegraphics{create}
  \caption{Control flow when creating a hsext object.}
  \label{F:create}
\end{figure}

Pd's API has several functions to create inlets and add methods, but in
hsext-0.1 an object has but one method (a design decision that later turned out
to be a mistake).  If the Creator returns an Instance successfully, hsext calls
out to libhsext to create a new Pd object with the required number of proxy
inlets (a technique used in Pd's ``list'' family of objects) and the required
number of outlets, then links the Pd object with the Instance and returns the
Pd object to the caller (the main Pd program).

When the Pd object receives a message at one of its proxy inlets, Pd calls a
function in libhsext.  This function looks into the inlet structure to find the
owning Instance ID, and passes all this information into the Haskell core of
hsext.  The core calls the one method defined in the Instance with these
arguments, and in addition passes in a structure exposing the PureData API.  In
hsext-0.1 this last is a function that can be used to send messages to the Pd
object's outlets (and thus back into the C environment of the Pd main program)
(Figure \ref{F:inlet}).

\begin{figure} 
  \centering
  \includegraphics{inlet}
  \caption{Control flow when data arrives at an hsext inlet.}
  \label{F:inlet}
\end{figure}

\section{Using hsext: a generic Swap object}

The simplest not-quite-trivial example showing most of the features of hsext-0.1
follows.  We want to build a Pd object that behaves like Pd's internal swap, but
that can operate on arbitrary messages, not just float messages.  We are going
to call it Swap, store it in a file called Swap.hs, and instantiate it in Pd
like [hsext Swap].

First, we need a module declaration:

\begin{verbatim}
module Swap where
\end{verbatim}
Next, we need to import the PureData API:
\begin{verbatim}
import PureData
\end{verbatim}
We need to preserve state between calls from Pd, and a simple way to do this is
using IORefs:
\begin{verbatim}
import Data.IORef
\end{verbatim}

Now we can define our creator resource, which creates a new initial state and
returns an Instance.  Notice how the state variable reference is embedded in the
returned method:
\begin{verbatim}
creator :: Creator
creator args = do
  bang <- gensym "bang"
  state <- newIORef (bang, [])
  return (Just Instance{
    iInlets  = 2,
    iOutlets = 2,
    iMethod  = inlet state})
\end{verbatim}

The left inlet recalls the stored state (a message), sends the incoming message
to the right outlet, and sends the recalled message to the left outlet:
\begin{verbatim}
inlet state outlet 0 s m = do
  (rs,rm) <- readIORef state
  outlet 1 s m
  outlet 0 rs rm
  return ()
\end{verbatim}
The right inlet simply stores the incoming message:
\begin{verbatim}
inlet state outlet 1 s m = do
  writeIORef state (s,m)
  return ()
\end{verbatim}


\section{The next level: towards hsext-1.0}

The PureData interface defined by hsext-0.1 is deficient, for several
reasons:

\begin{itemize}

\item The ``creator resource'' implies that a Pd object written in Haskell
cannot have a state shared between its instances \footnote{Haskell provides
functions such as unsafePerformIO which might be used to work around this, but
these are fraught with danger}.

\item The ``creator resource'' implies that each Haskell source file can define
only one Pd object class.

\item The ``one method for all inlets'' paradigm is mismatched to Pd's way of
doing things, making it awkward to have different behaviour for different
selectors.

\item Passing the ``outlet function'' to the inlet method is non-extensible.

\item An insufficient amount of Pd's functionality is accessible from Haskell.

\end{itemize}

These deficiencies could be rectified in the following ways:

\begin{itemize}

\item Replace the ``creator resource'' with a ``setup resource'', which hsext
will guarantee is executed exactly once (thus allowing state unique to each
source file).

\item Map to Pd's API more directly from hsext's PureData interface.  In
particular, wrap access to Pd object class creation, and introduce a ``one
method for each (inlet, selector) pair'' paradigm (while still allowing
``anything'' methods for all inlets).

\item Pass a Haskell record containing a collection of possible API calls to
each method, ensuring future extensibility without requiring hsext users to
modify their code.

\item Wrap more of Pd's API, including:
\begin{itemize}
  \item Clock callbacks.
  \item Receive names.
  \item Table access.
  \item Pointer atoms.
\end{itemize}

\end{itemize}

\section{Pipe dreams: sending Haskell data through Pd patch-cords}

TODO FIXME

\end{document}
